import gdal
import ogr
import osr
import os


def TransformGeometry(geometry, labels_sref):
    '''Returns cloned geometry, which is transformed to labels spatial reference'''
    geom_sref = geometry.GetSpatialReference()
    transform = osr.CoordinateTransformation(geom_sref, labels_sref)
    geom_trans = geometry.Clone()
    geom_trans.Transform(transform)
    return geom_trans


def CreatePolygonFromUpperLeft(ulx, uly, xres, yres):
    '''Creates a ogr polygon geometry based on upper left coordinate (x,y)'''
    ring = ogr.Geometry(ogr.wkbLinearRing)
    ring.AddPoint(ulx, uly)
    ring.AddPoint(ulx + xres, uly)
    ring.AddPoint(ulx + xres, uly - yres)
    ring.AddPoint(ulx, uly - yres)
    ring.AddPoint(ulx, uly)
    poly = ogr.Geometry(ogr.wkbPolygon)
    poly.AddGeometry(ring)
    return poly


def IntersectionCheck(geometry, geometrylist):
    ''' Takes a ogr geometry and a list of ogr geometries as input.
    The single geometry (first argument) will be tested against all
    geometries in the list. If all intersection checks return a False the
    function returns 0 (= no intersection found)'''

    if len(geometrylist) == 0:
        return 0
    else:
        checklist = []
        for i in range(0, len(geometrylist)):
            checklist.append(geometry.Intersect(geometrylist[i]))
        if any(checklist):
            return 1
        else:
            return 0

def CreatePolygonFromExtent(extent, spatialref):
    '''Creates polygon geometry from extent, i.e. list or tuple of [upper-left-x, upper-left-y,
    lower-right-x, lowerright-y]'''
    ulx, uly, lrx, lry  = extent[0], extent[1], extent[2], extent[3]
    ring = ogr.Geometry(ogr.wkbLinearRing)
    ring.AddPoint(ulx, uly)
    ring.AddPoint(lrx, uly)
    ring.AddPoint(lrx, lry)
    ring.AddPoint(ulx, lry)
    ring.AddPoint(ulx, uly)
    poly = ogr.Geometry(ogr.wkbPolygon)
    poly.AddGeometry(ring)
    poly.AssignSpatialReference(spatialref)
    return poly


def GeometryToShapefile(geometry, filepath):
    shpDriver = ogr.GetDriverByName("ESRI Shapefile")
    # Remove output shapefile if it already exists
    if os.path.exists(filepath):
        shpDriver.DeleteDataSource(filepath)
    outDataSource = shpDriver.CreateDataSource(filepath)
    outLayer = outDataSource.CreateLayer(filepath, srs=geometry.GetSpatialReference(), geom_type=geometry.GetGeometryType())
    featureDefn = outLayer.GetLayerDefn()
    outFeature = ogr.Feature(featureDefn)
    outFeature.SetGeometry(geometry)
    outLayer.CreateFeature(outFeature)
    outFeature = None


def SRSFromEPSG(epsg):
    if isinstance(epsg, int):
        outSpatialRef = osr.SpatialReference()
        outSpatialRef.ImportFromEPSG(epsg)
        return outSpatialRef
    else:
        print("Input not recognized as EPSG code (integer number)")


def ReprojectShapefile(infile, outfile, out_srs):
    # thanks to https://pcjericks.github.io/py-gdalogr-cookbook/projection.html#reproject-a-layer
    driver = ogr.GetDriverByName('ESRI Shapefile')

    # get the input layer
    inDataSet = driver.Open(infile)
    inLayer = inDataSet.GetLayer()
    # input SpatialReference
    inSpatialRef = inLayer.GetSpatialRef()
    # output SpatialReference
    outSpatialRef = out_srs
    # create the CoordinateTransformation
    coordTrans = osr.CoordinateTransformation(inSpatialRef, outSpatialRef)

    # create the output layer
    outputShapefile = outfile
    if os.path.exists(outputShapefile):
        driver.DeleteDataSource(outputShapefile)
    outDataSet = driver.CreateDataSource(outputShapefile)
    outLayer = outDataSet.CreateLayer(outfile, srs=out_srs, geom_type=inLayer.GetGeomType())

    # add fields
    inLayerDefn = inLayer.GetLayerDefn()
    for i in range(0, inLayerDefn.GetFieldCount()):
        fieldDefn = inLayerDefn.GetFieldDefn(i)
        outLayer.CreateField(fieldDefn)

    # get the output layer's feature definition
    outLayerDefn = outLayer.GetLayerDefn()

    # loop through the input features
    inFeature = inLayer.GetNextFeature()
    while inFeature:
        # get the input geometry
        geom = inFeature.GetGeometryRef()
        # reproject the geometry
        geom.Transform(coordTrans)
        geom.AssignSpatialReference(out_srs)
        # create a new feature
        outFeature = ogr.Feature(outLayerDefn)
        # set the geometry and attribute
        outFeature.SetGeometry(geom)
        for i in range(0, outLayerDefn.GetFieldCount()):
            outFeature.SetField(outLayerDefn.GetFieldDefn(i).GetNameRef(), inFeature.GetField(i))
        # add the feature to the shapefile
        outLayer.CreateFeature(outFeature)
        # dereference the features and get the next input feature
        outFeature = None
        inFeature = inLayer.GetNextFeature()

    # Save and close the shapefiles
    inDataSet = None
    outDataSet = None
