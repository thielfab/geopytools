from osgeo import gdal, ogr, osr
from geopytools.vector import TransformGeometry
import numpy as np
import struct


def GetBoundaryCoordinates(inRaster):
    gt = inRaster.GetGeoTransform()
    # upper left
    ulx, uly = gt[0], gt[3]
    # lower right
    lrx = ulx + (gt[1] * inRaster.RasterXSize)
    lry = uly + (gt[5] * inRaster.RasterYSize)
    # upper right
    urx = lrx
    ury = uly
    # lower left
    llx = ulx
    lly = lry
    return [ulx, uly, lrx, lry, urx, ury, llx, lly]


def GetCommonExtent(infiles):
    '''Takes a list of raster files and returns tuple of common extent coordinates (
    ulx, uly, lrx, lry'''
    allcoords = []
    for raster in infiles:
        ds = gdal.Open(raster, gdal.GA_ReadOnly)
        coords = GetBoundaryCoordinates(ds)
        allcoords.append(coords)

    # Assess common extent of all inputs
    ulx = max([coords[0] for coords in allcoords]) #max value throughout all ulx coordinates
    uly = min([coords[1] for coords in allcoords]) #min value throughout all uly coordinates
    lrx = min([coords[2] for coords in allcoords])
    lry = max([coords[3] for coords in allcoords])

    return [ulx, uly, lrx, lry]


def GetMaximumExtent(infiles):
    '''Takes a list of raster files and returns tuple of common extent coordinates (
    ulx, uly, lrx, lry'''
    allcoords = []
    for raster in infiles:
        ds = gdal.Open(raster, gdal.GA_ReadOnly)
        coords = GetBoundaryCoordinates(ds)
        allcoords.append(coords)

    # Assess common extent of all inputs
    ulx = min([coords[0] for coords in allcoords]) #max value throughout all ulx coordinates
    uly = max([coords[1] for coords in allcoords]) #min value throughout all uly coordinates
    lrx = max([coords[2] for coords in allcoords])
    lry = min([coords[3] for coords in allcoords])

    return [ulx, uly, lrx, lry]


def ReadRasterBandSubsetAsArray(inRaster, band, extent):
    ## Props to Chris Garrard
    ulx, uly, lrx, lry = extent[0], extent[1], extent[2], extent[3]
    in_ds = gdal.Open(inRaster, gdal.GA_ReadOnly)
    in_gt = in_ds.GetGeoTransform()
    inv_gt = gdal.InvGeoTransform(in_gt)

    offsets_ul = gdal.ApplyGeoTransform(inv_gt, ulx, uly)
    offsets_lr = gdal.ApplyGeoTransform(inv_gt, lrx, lry)
    # off_ulx, off_uly = map(lambda x:int(round(x)), offsets_ul)
    # off_lrx, off_lry = map(lambda x:int(round(x)), offsets_lr)
    off_ulx, off_uly = map(int, offsets_ul)
    off_lrx, off_lry = map(int, offsets_lr)
    rows = off_lry - off_uly
    columns = off_lrx - off_ulx
    return in_ds.GetRasterBand(band).ReadAsArray(off_ulx, off_uly, columns, rows)


def SpatialReferenceFromRaster(ds):
    '''Returns SpatialReference from raster dataset'''
    if isinstance(ds, str):
        ds = gdal.Open(ds)
    else:
        ds = ds

    pr = ds.GetProjection()
    sr = osr.SpatialReference()
    sr.ImportFromWkt(pr)
    return sr


def ExtractRasterValueAtLocation(point, file):
    '''Returns value(s) of raster band(s) at a certain location as a list.

    Parameters:
    -----------
    point: OGR point Geometry
    file: either path to gdal file or gdal dataset object

    '''
    dtype_dict = {1: 'b', 2: 'H', 3: 'h', 4: 'I', 5: 'i', 6: 'f', 7: 'd'}

    if isinstance(file, str):
        ds = gdal.Open(file)
    else:
        ds = file

    point_sref = point.GetSpatialReference()
    ds_sref = SpatialReferenceFromRaster(ds)
    if point_sref.IsSame(ds_sref) == 1:
        pass
    else:
        point = TransformGeometry(point, ds_sref)

    mx, my = point.GetX(), point.GetY()
    gt = ds.GetGeoTransform()
    px = int((mx - gt[0]) / gt[1])
    py = int((my - gt[3]) / gt[5])

    nr_bands = ds.RasterCount
    if nr_bands > 1:
        values = []
        for i in range(nr_bands):
            rb = ds.GetRasterBand(i + 1)
            struc_var = rb.ReadRaster(px, py, 1, 1)
            val = struct.unpack(dtype_dict[rb.DataType], struc_var)
            values.append(val[0])
        return values
    else:
        rb = ds.GetRasterBand(1)
        struc_var = rb.ReadRaster(px, py, 1, 1)
        val = struct.unpack(dtype_dict[rb.DataType], struc_var)
        return [val[0]]


def WriteArrayToRaster(array, newRasterfn, rasterOrigin, pixelWidth, pixelHeight, projection):
    # adapted from https://pcjericks.github.io/py-gdalogr-cookbook/raster_layers.html#create-raster-from-array
    cols = array.shape[1]
    rows = array.shape[0]
    originX = rasterOrigin[0]
    originY = rasterOrigin[1]

    driver = gdal.GetDriverByName("GTiff")
    outRaster = driver.Create(newRasterfn, cols, rows, 1, gdal.GDT_Byte)
    outRaster.SetGeoTransform((originX, pixelWidth, 0, originY, 0, pixelHeight))
    outband = outRaster.GetRasterBand(1)
    outband.WriteArray(array)
    outRaster.SetProjection(projection)
    outband.FlushCache()


def QAtoFmask(qa):
    fmask = np.full_like(qa, fill_value=np.iinfo(np.ubyte).max)
    fmask[np.where(np.bitwise_and(qa, 2) != 0)] = 0  # clear
    fmask[np.where(np.bitwise_and(qa, 4) != 0)] = 1  # water
    fmask[np.where(np.bitwise_and(qa, 8) != 0)] = 2  # cloud shadow
    fmask[np.where(np.bitwise_and(qa, 16) != 0)] = 3  # snow
    fmask[np.where(np.bitwise_and(qa, 32) != 0)] = 4  # cloud
    return fmask


def StackL8C1_SR(indir, outdir, mask=None, write_mask=None):
    from os import path, listdir
    from re import search
    files = [path.join(indir, f) for f in listdir(indir) if f.endswith('.tif')]

    driver = gdal.GetDriverByName('GTiff')
    driveropts = ["COMPRESS=LZW", "PREDICTOR=2", "BIGTIFF=IF_NEEDED", "NUM_THREADS=5", "INTERLEAVE=BAND"]
    names = ['blue_sr', 'green_sr', 'red_sr', 'nir_sr', 'swir1_sr', 'swir2_sr']

    qa_ds = gdal.Open([a for a in files if search('pixel_qa.tif', a)][0])
    qa_b = qa_ds.GetRasterBand(1)
    qa_arr = qa_b.ReadAsArray()
    fmask = QAtoFmask(qa_arr)

    sr = {path.basename(f): gdal.Open(f).ReadAsArray() for f in files if search('_sr_band[^1]', f)}
    sr_arr = np.dstack([np.atleast_3d(a) for a in sr.values()])

    if mask == None:
        sr_arr[sr_arr == -9999] = np.iinfo(np.int16).max

    if mask == True:
        sr_arr[np.where(fmask != 0)] = np.iinfo(np.int16).max

    out_ds = driver.Create(path.join(outdir, f'{path.basename(indir)}_stack.tif'), qa_ds.RasterXSize,
                           qa_ds.RasterYSize, sr_arr.shape[2], gdal.GDT_Int16, options=driveropts)
    out_ds.SetGeoTransform(qa_ds.GetGeoTransform())
    out_ds.SetProjection(qa_ds.GetProjection())
    out_naflag = np.iinfo(np.int16).max

    for i in range(sr_arr.shape[2]):
        out_band = out_ds.GetRasterBand(i + 1)
        out_band.WriteArray(sr_arr[:, :, i])
        out_band.SetNoDataValue(out_naflag)
        out_band.ComputeStatistics(True)
        out_band.SetDescription(names[i])
        out_band.FlushCache()

    if write_mask == True:
        out_fmask = driver.Create(path.join(outdir, f'{path.basename(indir)}_fmask.tif'), qa_ds.RasterXSize,
                                  qa_ds.RasterYSize, 1, gdal.GDT_Byte, options=driveropts)
        out_fmask.SetGeoTransform(qa_ds.GetGeoTransform())
        out_fmask.SetProjection(qa_ds.GetProjection())
        out_band = out_fmask.GetRasterBand(1)
        out_band.WriteArray(fmask)
        out_band.SetNoDataValue(np.iinfo(np.ubyte).max)
        out_band.SetDescription('fmask')
        out_band.FlushCache()
